
from __future__ import print_function


import datetime
import time
import pendulum


def spew( *args ):
    items = []
    for a in args:
        items.append( a.isoformat() )
        items.append( a.timestamp() )
    print( '\t'.join( [ str(f) for f in items ] ) )


while True:
    # spew( datetime.datetime.now(), datetime.datetime.utcnow(), pendulum.now( 'local' ) )
    spew( datetime.datetime.now(), pendulum.from_timestamp( time.time() ).in_tz( 'local' ) )
    time.sleep( 1 )


