# dst - daylight saving time test

Goal - verify proper transition across dst transtions.

Spring 2019:
2019-03-10T01:59:59-05:00 to 03:00:00-04:00
sudo date -s --date '@1552201199'

Fall 2019:
2019-11-03T00:59:59-04:00 to 01:00:00-04:00
This has been problematic with pendulum, which jumps an hour.
sudo date -s --date '@1572757199'

2019-11-03T01:59:59-04:00 to 01:00:00-05:00
this is the true transition
sudo date -s --date '@1572760799'


